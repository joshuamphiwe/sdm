<?php

spl_autoload_register(function ($class) {
    $sources = array("../functions/$class.php", "../config/$class.php");

    foreach ($sources as $source) {
        if (file_exists($source)) {
            require_once $source;
        }
    }
});

$UserInfo = new UserInfo();
$getUserInfo = $UserInfo->getUserInfo();

?>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
</head>

<body>
    <div class="container py-3">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-3">SDM DEMO</h2>
                <nav class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group" role="group">
                        <a class="btn btn-primary active" href="add_info.php">Add User Info</a>
                        <a class="btn btn-primary" href="view_info.php">View User Info</a>
                    </div>
                </nav>
                <hr class="mb-4">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <span class="anchor"></span>
                        <div class="card card-outline-secondary">
                            <div class="card-header">
                                <h3 class="mb-0">View User Info</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email Address</th>
                                            <th>Phone Number</th>
                                            <th>Extra Info</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php

                                        if($getUserInfo['Code'] == '00' && count($getUserInfo['Data']) > 0){
                                            $data = $getUserInfo['Data'];

                                            foreach($data as $key=>$item){

                                                $user_id = $item['ID'];
                                                $user_name = $item['Name'];
                                                $user_email = $item['Email'];
                                                $user_phone = $item['Phone'];
                                                $user_data = $item['MetaData'];

                                                $stringData = "";
                                                foreach($user_data as $key2=>$item2){
                                                    $key2 = str_replace("_"," ", $key2);
                                                    $stringData .= $key2." = ".$item2."<br/>";
                                                }

                                                echo "<tr>";
                                                echo "<td>#" . $user_id. "</td>";
                                                echo "<td>" . $user_name. "</td>";
                                                echo "<td>" . $user_email. "</td>";
                                                echo "<td>" . $user_phone. "</td>";
                                                echo "<td>" . $stringData. "</td>";
                                                echo "</tr>";

                                            }

                                        }

                                    ?>
                                    <tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>