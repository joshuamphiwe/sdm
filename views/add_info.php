<?php

    spl_autoload_register ( function ($class) {
        $sources = array("../functions/$class.php","../config/$class.php");

        foreach ($sources as $source) {
            if (file_exists($source)) {
                require_once $source;
            } 
        } 
    });

    if(isset($_POST['name'])){
        
        $UserInfo = new UserInfo();
        $addUserInfo = $UserInfo->addUserInfo();

        if($addUserInfo['Code'] == '00'){
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal.fire("'.$addUserInfo['Msg'].'","Done!","success");';
            echo '}, 500);</script>';
        } else if($addUserInfo['Code'] == '01') {
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal.fire("'.$addUserInfo['Msg'].'","Something Wrong!","error");';
            echo '}, 500);</script>';
        } else {
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal.fire("ERROR!","Something Wrong!","error");';
            echo '}, 500);</script>';
        }

    }

?>
<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="../plugins/sweetalert2/sweetalert2.min.css">
    <script src="../plugins/sweetalert2/sweetalert2.min.js"></script>
</head>

<body>
    <div class="container py-3">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-3">SDM DEMO</h2>
                <nav class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group" role="group">
                        <a class="btn btn-primary active" href="add_info.php">Add User Info</a>
                        <a class="btn btn-primary" href="view_info.php">View User Info</a>
                    </div>
                </nav>
                <hr class="mb-4">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <span class="anchor"></span>
                        <div class="card card-outline-secondary">
                            <div class="card-header">
                                <h3 class="mb-0">User Info</h3>
                            </div>
                            <div class="card-body">
                                <form autocomplete="off" class="form" role="form" method="POST">
                                    <div class="form-group">
                                        <label for="name">Name <span class="required_field">*</span></label>
                                        <input class="form-control" id="name" name="name" required type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_address">Email Address <span class="required_field">*</span></label>
                                        <input class="form-control" id="email_address" name="email_address" required type="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number <span class="required_field">*</span></label>
                                        <input class="form-control" id="phone_number" name="phone_number" required type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="Date of Birth">Date of Birth</label>
                                        <input class="form-control" name="Date of Birth" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="Gender">Gender</label>
                                        <input class="form-control" name="Gender" type="text">
                                    </div>
                                    <button class="btn btn-success" type="submit">Add User Info</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <span class="anchor"></span>
                        <div class="card card-outline-secondary">
                            <div class="card-header">
                                <h3 class="mb-0">User Info</h3>
                            </div>
                            <div class="card-body">
                                <form autocomplete="off" class="form" role="form" method="POST">
                                    <div class="form-group">
                                        <label for="name">Name <span class="required_field">*</span></label>
                                        <input class="form-control" id="name" name="name" required type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="email_address">Email Address <span class="required_field">*</span></label>
                                        <input class="form-control" id="email_address" name="email_address" required type="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number <span class="required_field">*</span></label>
                                        <input class="form-control" id="phone_number" name="phone_number" required type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="Car Make">Car Make</label>
                                        <input class="form-control" name="Car Make" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="Car Color">Car Color</label>
                                        <input class="form-control" name="Car Color" type="text">
                                    </div>
                                    <button class="btn btn-success" type="submit">Add User Info</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>