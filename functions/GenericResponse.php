<?php

    class GenericResponse {
        
        function response($status, $code, $msg, $data){
            return array("Status" => $status, "Code" => $code, "Msg" => $msg, "Data" => $data);
        }

        function getRawPostData(){
            $obj = $_POST;
            return $obj;
        }

    }

?>