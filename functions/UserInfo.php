<?php

    set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
        if (0 === error_reporting()) {
            return false;
        }
    
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    });

    class UserInfo extends GenericResponse {
        
        private $con;
        
        function __construct() {
            $this->DataConnectClass = new DataConnectClass();
            $this->con = $this->DataConnectClass->getCon();
        }
        
        function addUserInfo(){
            
            $this->body = $this->getRawPostData();
            
            if(empty($this->body)){
                return $this->response("ERROR", "01", "Invalid body.", []);
            }

            $this->name = isset($this->body['name']) ? $this->body['name'] : '';
            $this->email = isset($this->body['email_address']) ? $this->body['email_address'] : '';
            $this->phone = isset($this->body['phone_number']) ? $this->body['phone_number'] : '';

            if($this->name == ''){
                return $this->response("ERROR", "01", "Please enter name.", []);
            }

            if($this->email == ''){
                return $this->response("ERROR", "01", "Please enter name.", []);
            }

            if($this->phone == ''){
                return $this->response("ERROR", "01", "Please enter name.", []);
            }

            if(!($this->isValidEmail())){
                return $this->response("ERROR", "01", "Email Address is invalid.", []);
            }

            $this->cleanPhoneNumber();

            $this->user_id = $this->insertUser();

            if($this->user_id != ""){

                $this->data = $this->getUserMetaData();

                $this->insertMetaData();

                return $this->response("SUCCESS", "00", "User info added successful.", $this->user_id);
            }
            
            return $this->response("ERROR", "00", "Failed to add info.", []);
            
        }

        function insertUser(){

            $name = $this->name;
            $email = $this->email;
            $phone = $this->phone;

            $sql = "INSERT INTO `user_info` 
                    (`user_id`, `user_name`, `user_email`, `user_phone`) 
                    VALUES 
                    (NULL, '$name', '$email', '$phone')";

            mysqli_query($this->con, $sql);

            if(mysqli_affected_rows($this->con)){
                return mysqli_insert_id($this->con);
            } else {
                return "";
            }

        }

        function insertMetaData(){

            $user_id = $this->user_id;

            foreach($this->data as $key=> $value){

                $sql = "INSERT INTO `user_meta_data` 
                        (`meta_id`, `meta_user`, `meta_key`, `meta_value`) 
                        VALUES 
                        (NULL, '$user_id', '$key', '$value')";

                mysqli_query($this->con, $sql);

            }

        }

        function getUserMetaData(){
            $data = [];
            foreach($this->body as $key=> $value){
                if($key != 'name' && $key != 'email_address' && $key != 'phone_number' && $value != ''){
                    $data[$key] = $value;
                }
            }
            return $data;
        }
        
        function getUserInfo(){
            
            $sql = "SELECT u.user_id, u.user_name, u.user_email, u.user_phone FROM user_info u";
            $res = mysqli_query($this->con, $sql);

            $data = [];

            if(mysqli_num_rows($res) > 0){

                while($row = mysqli_fetch_assoc($res)){

                    $user_id = $row['user_id'];
                    $user_name = $row['user_name'];
                    $user_email = $row['user_email'];
                    $user_phone = $row['user_phone'];

                    $meta = [];

                    $sqlMeta = "SELECT m.meta_key, m.meta_value FROM user_meta_data m WHERE m.meta_user LIKE '$user_id'";
                    $resMeta = mysqli_query($this->con, $sqlMeta);

                    if(mysqli_num_rows($resMeta) > 0){
                        while($rowMeta = mysqli_fetch_assoc($resMeta)){
                            $meta_key = $rowMeta['meta_key'];
                            $meta_value = $rowMeta['meta_value'];
                            $meta[$meta_key] = $meta_value;
                        }
                    }

                    $data[] = ["ID" => $user_id, "Name"=>$user_name, "Email"=>$user_email, "Phone"=>$user_phone,"MetaData" => $meta];

                }

            }
            
            return $this->response("SUCCESS", "00", "Data.", $data);
            
        }

        function isValidEmail(){
            $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
            
            if (preg_match($pattern, $this->email) === 1) {
                return true;
            }

            return false;

        }

        function cleanPhoneNumber(){

            $this->phone = preg_replace( '/[^0-9]/i', '', $this->phone);

            if(substr($this->phone,0,2) == "27"){
                $this->phone = "0".substr($this->phone,2,strlen($this->phone)-2);
            }

        }
        
    }
    
?>