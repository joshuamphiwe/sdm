<?php

    class DataConnectClass{
        
        private $con;
        
        private function doConnection(){
            $databaseName = "sdm_demo";
            $databaseUser = "root";
            $databasePassword = "";
            $databaseServer = "localhost";
            
            $dbConnect = mysqli_connect($databaseServer,$databaseUser,$databasePassword,$databaseName);
            
            if (mysqli_connect_errno()){
                $statusCode = 503;
                http_response_code($statusCode);
            	echo "Service is unavailable.";
            	exit;
            }
            
            $this->con = $dbConnect;
            
        }
        
        public function getCon(){
            $this->doConnection();
            return $this->con;
        }
        
        
    }


?>